import { KeybindConfigurator } from "./configurator";
import { KeyEvent, AnyEventFormat, matchEvent } from "./key-utils";

interface RegistrationOptions {
  name?: string;
  hint?: string;
  default?: AnyEventFormat;
  config?: boolean;
  onKeyDown?: (evt: KeyEvent) => void;
  onKeyUp?: (evt: KeyEvent) => void;
  scope?: "client" | "world";
  type?;
}

class RegisteredKeybind {
  moduleName: string;
  bindName: string;

  options: RegistrationOptions;
  fullName: string;

  public get keyBind(): KeyEvent {
    const setting = game.settings.get(this.moduleName, this.bindName) as string;
    if (setting) {
      return KeyEvent.fromString(setting);
    }
  }

  constructor(moduleName, bindName, options: RegistrationOptions) {
    this.moduleName = moduleName;
    this.bindName = bindName;
    this.fullName = `${moduleName}.${bindName}`;
    this.options = { ...options };
    delete this.options.name;
  }
}

const defaultOptions: RegistrationOptions = {
  scope: "client",
  config: false,
  type: String,
};

class KeybindLib {
  static registeredBinds = new Map<string, RegisteredKeybind>();

  static isBoundTo(
    evt: AnyEventFormat,
    moduleName: string,
    bindName: string
  ): boolean {
    const bind = this.registeredBinds.get(`${moduleName}.${bindName}`);
    if (!bind) return false;
    return matchEvent(evt, bind.keyBind);
  }

  static register(
    moduleName: string,
    bindName: string,
    options: RegistrationOptions
  ): void {
    if (!game.modules.get(moduleName) && moduleName !== game.system.id) {
      throw new Error(`Module or system does not exist: ${moduleName}`);
    }
    const fullName = `${moduleName}.${bindName}`;
    const opt = { ...defaultOptions, ...options, fullName };

    if (this.registeredBinds.has(fullName))
      throw new Error(
        `Keybind already registered, module: ${moduleName}, bindName: ${bindName}`
      );

    const setting = game.settings.settings.get(`${moduleName}.${bindName}`);
    if (setting) {
      if (setting.type != String) {
        throw new Error(
          `Keybind Lib | Setting for keybind already registered, needs to be String. Incorrect type: ${setting.type}, module: ${moduleName}, bindName: ${bindName}`
        );
      }
      console.info(
        `Keybind Lib | Setting for keybind already registered, will be used; module: ${moduleName}, bindName: ${bindName}`
      );
    } else {
      game.settings.register(moduleName, bindName, {
        ...(options as ClientSettings.PartialSetting),
        type: String,
      });
    }

    this.registeredBinds.set(
      fullName,
      new RegisteredKeybind(moduleName, bindName, opt)
    );
  }

  // UI methods for configurator conflicts
  static _resolveConflicts(html: JQuery): void {
    // Reset visuals
    const inputs = html.find(".keybind-input") as JQuery<HTMLInputElement>;
    inputs.removeClass("keybind-conflict");
    inputs.parent().find(".conflict-info").detach();

    // Collect all on-screen inputs
    const values: { [key: string]: KeyEvent } = {};
    inputs.each(function () {
      values[this.getAttribute("name")] = KeyEvent.fromString(this.value);
    });

    // Collect all off-screen keybinds
    // KeybindLib.registeredBinds.entries();
    // for (const [fullName, reg] of KeybindLib.registeredBinds.entries()) {
    //   if (!values[fullName]) {
    //     values[fullName] = reg.keyBind;
    //   }
    // }

    // Find all conflicts
    const vals = Object.entries(values);
    const conflicts: { [fn: string]: string[] } = {};
    vals.forEach(([fn, e]) => {
      for (const [fn2, e2] of vals) {
        if (fn != fn2 && e.equals(e2)) {
          conflicts[fn] = conflicts[fn] || [];
          conflicts[fn].push(fn2);
        }
      }
    });

    // Modify DOM
    Object.entries(conflicts).forEach(([fn, conflicts]) => {
      const input = $(html).find(
        `input[name="${fn}"]`
      ) as JQuery<HTMLInputElement>;

      if (input.length == 0) return;

      const conflictTitles = conflicts.map(
        (fn) => KeybindLib.registeredBinds.get(fn).options.name
      );

      let conflictText = conflictTitles
        .map((title) => game.i18n.localize(title))
        .join(", ");
      conflictText = game.i18n.format("KEYBINDLIB.ConflictsWith", {
        conflicts: conflictText,
      });
      input.addClass("keybind-conflict");
      input.after(`<span class="hint conflict-info">${conflictText}</span>`);
    });
  }

  static _handleConfigInput(evt: JQuery.KeyboardEventBase, html: JQuery): void {
    const keyEvent = KeyEvent.fromEvent(evt);
    if (!keyEvent) return;
    evt.preventDefault();
    evt.stopPropagation();
    evt.target.value = keyEvent.toString();
    this._resolveConflicts(html);
  }

  private static _onKeyDown(evt: KeyboardEvent): void {
    const keyEvent = KeyEvent.fromEvent(evt);
    if (!keyEvent) return;
    for (const [bindName, bind] of this.registeredBinds.entries()) {
      if (bind.keyBind.equals(keyEvent)) {
        console.log("Keybind Lib | Key down", bindName);
        bind.options?.onKeyDown?.(keyEvent);
      }
    }
  }

  private static _onKeyUp(evt: KeyboardEvent): void {
    const keyEvent = KeyEvent.fromEvent(evt);
    if (!keyEvent) return;
    for (const [bindName, bind] of this.registeredBinds.entries()) {
      if (bind.keyBind.equals(keyEvent)) {
        console.log("Keybind Lib | Key up", bindName);
        bind.options?.onKeyUp?.(keyEvent);
      }
    }
  }

  static _registerEventListeners(): void {
    document.addEventListener("keydown", this._onKeyDown.bind(KeybindLib));
    document.addEventListener("keyup", this._onKeyUp.bind(KeybindLib));
  }
}

function registerMenu(): void {
  if (KeybindLib.registeredBinds.size === 0) return;

  let bindsNotInConfig = false;
  KeybindLib.registeredBinds.forEach(
    (bind) => (bindsNotInConfig = bindsNotInConfig || !bind.options.config)
  );

  if (!bindsNotInConfig) return;

  game.settings.registerMenu("keybind-lib", "keybinds", {
    name: "KEYBINDLIB.MenuName",
    label: "KEYBINDLIB.MenuLabel",
    icon: "fas fa-keyboard",
    type: KeybindConfigurator,
    restricted: false,
  });
}

Hooks.on("ready", () => {
  KeybindLib._registerEventListeners();

  // Allow time for modules to register if they use the ready hook
  setTimeout(registerMenu, 200);
});

Hooks.on("renderSettingsConfig", (app, html: JQuery<HTMLElement>) => {
  for (const [fullName] of KeybindLib.registeredBinds.entries()) {
    const input = $(html).find(
      `input[name="${fullName}"]`
    ) as JQuery<HTMLInputElement>;
    input.attr("readonly", "true");
    input.addClass("keybind-input");
    input.parent().addClass("keybind-fields");
    input
      .parent()
      .append(`<i class="far fa-keyboard keybind-lib-settings-icon"></i>`);
    input.on("keydown", (evt) => KeybindLib._handleConfigInput(evt, html));
  }

  KeybindLib._resolveConflicts(html);
});

globalThis.KeybindLib = KeybindLib;
export { KeybindLib };

if (__buildEnv__ != "production") {
  import("../test/unit/tests").then((tests) => tests.registerTests());
}
