## [0.3.4] - 2021-08-29

Hotfix!

### Fixed

- Keybind lib stopped working everywhere

## [0.3.3] - 2021-08-29

Some overdue fixes

### Added

- Spanish translation

### Changed

- Usable by systems, not only modules
- Compatible with 0.8.8

## [0.3.2] - 2021-06-01

Cleaning and 0.8.6 compatibility

### Changed

- Compatible with 0.8.6

## [0.3.0] - 2021-04-05

Robustness and polishing

### Added

- Keybind conflict detection

### Changed

- If all keybinds are in the regular settings, there's no separate menu
- Bump compatibility to 0.7.9

## [0.2.0] - 2021-01-20

Breaking change!

### Changed

- Replaced `callback` function with `onKeyDown` and `onKeyUp`

## [0.1.0] - 2021-01-20

First release
