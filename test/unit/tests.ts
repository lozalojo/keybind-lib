// Simple test runner for manual testing
import { KeyEvent } from "../../src/key-utils";

const testKeys = {
  "Ctrl + Shift + KeyA": KeyEvent.fromObject({
    key: "KeyA",
    ctrlKey: true,
    shiftKey: true,
  }),
  "Shift + KeyA": KeyEvent.fromObject({ key: "KeyA", shiftKey: true }),
  KeyA: KeyEvent.fromObject({ key: "KeyA" }),
};

export function registerTests(): void {
  Hooks.on("quenchReady", (quench) => {
    quench.registerBatch(
      "keybind-lib.key-utils",
      (context) => {
        const { describe, it, assert } = context;
        const { expect } = chai;

        describe("Keybind Conversions", function () {
          it("should convert correct strings to keybinds and back", function () {
            for (const [bindString, expectedEvent] of Object.entries(
              testKeys
            )) {
              const keyEvent = KeyEvent.fromString(bindString);
              assert.equal(keyEvent.toString(), bindString);
              assert.equal(expectedEvent.toString(), bindString);
              assert.ok(keyEvent.equals(expectedEvent));
              assert.ok(expectedEvent.equals(keyEvent));
            }
          });

          it("should not care about term order", function () {
            const result = KeyEvent.fromObject({
              key: "KeyA",
              ctrlKey: true,
              shiftKey: true,
            });
            assert.ok(
              result.equals(KeyEvent.fromString("Ctrl + Shift + KeyA"))
            );
            assert.ok(
              result.equals(KeyEvent.fromString("Shift + Ctrl + KeyA"))
            );
            assert.ok(
              result.equals(KeyEvent.fromString("KeyA + Shift + Ctrl"))
            );
          });

          it("should not parse incorrect keybind strings", function () {
            expect(() => KeyEvent.fromString("KeyA + KeyA")).to.throw();
            expect(() => KeyEvent.fromString("KeyA KeyA")).to.throw();
            expect(() => KeyEvent.fromString("KeyA KeyB")).to.throw();
            expect(() => KeyEvent.fromString("Shift KeyA")).to.throw();
            expect(() => KeyEvent.fromString("A")).to.throw();
            expect(() => KeyEvent.fromString("keyA")).to.throw();
            expect(() => KeyEvent.fromString("")).to.throw();
            expect(() => KeyEvent.fromString(" ")).to.throw();
            expect(() => KeyEvent.fromString("Key-A")).to.throw();
            expect(() => KeyEvent.fromString("Key A")).to.throw();
            expect(() => KeyEvent.fromString("Ctrl +")).to.throw();
            expect(() => KeyEvent.fromString("+")).to.throw();
            expect(() => KeyEvent.fromString("Unidentified")).to.throw();
          });

          it("should not convert incorrect keybind objects", function () {
            expect(() => KeyEvent.fromObject({} as any)).to.throw();
            expect(() => KeyEvent.fromObject({ key: "A" })).to.throw();
          });
        });
      },
      { displayName: "Keybind Lib: Tests" }
    );
  });
}
