const fs = require("fs-extra");
const path = require("path");
const chalk = require("chalk");
const gulp = require("gulp");

const rollup = require("rollup");
const rollupTypescript = require("@rollup/plugin-typescript");
const rollupResolve = require("@rollup/plugin-node-resolve").nodeResolve;
const rollupReplace = require("@rollup/plugin-replace");

const buildTools = require("build-tools");

// Configuration

// Configuration
const options = {
  outDir: path.resolve("dist"),
  manifest: "./src/module.json",
  tsEntrypoint: "./src/keybind-lib.ts",
};

// Patterns for watch & compile
const sourceGroups = {
  ts: ["src/**/*.ts", "test/**/*.ts"],
  less: ["src/**/*.less"],
  sass: ["src/**/*.scss"],

  // Folders are copied as-is
  folders: ["templates", "lang"],
  // Files are copied following pattern
  statics: ["src/**/*.css"],
};

const packageTool = new buildTools.PackageTool(options);

/**
 * Build TypeScript
 */
let rollupCache;
async function buildTS() {
  const bundle = await rollup.rollup({
    input: options.tsEntrypoint,
    cache: rollupCache,
    preserveEntrySignatures: "allow-extension",
    plugins: [
      rollupReplace({
        __buildEnv__: JSON.stringify(process.env.NODE_ENV),
        preventAssignment: true,
      }),
      rollupTypescript(),
      rollupResolve(),
    ],
    manualChunks(id) {
      if (id.includes("node_modules")) {
        return "vendor";
      }
    },
  });

  rollupCache = bundle.cache;

  await bundle.write({
    dir: options.outDir,
    format: "es",
    preferConst: true,
    minifyInternalExports: false,
    exports: "auto",
    chunkFileNames: "[name].js",
    sourcemap: true,
  });
}

/**
 * Copy static files
 */
async function copyFolders() {
  try {
    for (const folder of sourceGroups.folders) {
      if (fs.existsSync(folder)) {
        await fs.copy(folder, path.join(options.outDir, folder));
      }
    }
    return Promise.resolve();
  } catch (err) {
    Promise.reject(err);
  }
}

async function copyStatics() {
  return gulp
    .src(sourceGroups.statics, { allowEmpty: true })
    .pipe(gulp.dest(options.outDir));
}

/**
 * Remove all files from `dist`
 */
async function clean() {
  if (!fs.existsSync(options.outDir)) {
    return Promise.resolve();
  }

  const files = await fs.readdir(options.outDir);
  console.log(" ", chalk.yellow("Files to clean:"));
  console.log("   ", chalk.blueBright(files.join("\n    ")));

  await Promise.all(
    files.map((filePath) => fs.remove(path.join(options.outDir, filePath)))
  );
}

/**
 * Watch for changes for each build step
 */
function buildWatch() {
  const opts = { ignoreInitial: false };
  gulp.watch(options.manifest, opts, packageTool.buildManifest);
  gulp.watch(sourceGroups.ts, opts, buildTS);
  gulp.watch(sourceGroups.folders, opts, copyFolders);
  gulp.watch(sourceGroups.statics, opts, copyStatics);
}

const execBuild = gulp.parallel(
  packageTool.buildManifest,
  buildTS,
  copyFolders,
  copyStatics
);

// Single tasks
exports.clean = clean;
exports.link = () => buildTools.linkUserData(options.manifest, options.outDir);
exports.unlink = () => buildTools.unlinkUserData(options.manifest);
exports.package = packageTool.package;
// Combined tasks
exports.build = execBuild;
exports.watch = buildWatch;
exports.publish = gulp.series(clean, execBuild, packageTool.package);
